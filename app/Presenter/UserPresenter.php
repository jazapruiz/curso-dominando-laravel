<?php

namespace App\Presenter;

use App\User;
use Illuminate\Support\HtmlString;

/**
 * 
 */
class UserPresenter extends Presenter
{
	public function link()
	{
		return new HtmlString("<a href='" . route('usuarios.show', $this->model->id) . "'>" . $this->model->name . "</a>");
	}

	public function roles()
	{
		return $this->model->roles->pluck('display_name')->implode(' - ');
		// {{--   ^   --}}
		// {{--   |   --}}
		// {{-- Esto es igual a esto --}}
		// {{-- 				   | --}}
		// {{-- 				   v --}}
		// {{-- <ul class="list-inline">
		// 	@foreach ($user->roles as $role)
		// 		<li class="list-inline-item">{{$role->display_name}}</li>
		// 	@endforeach
		// </ul> --}}
	}

	public function notes()
	{
		return $this->model->note ? $this->model->note->body : '';
	}

	public function tags()
	{
		return $this->model->tags ? $this->model->tags->pluck('name')->implode(', ') : '';
	}
}