<?php

namespace App\Listeners;

use Mail;
use App\Events\MessageWasReceived;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNotificationToTheOwner implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MessageWasReceived  $event
     * @return void
     */
    public function handle(MessageWasReceived $event)
    {
        // var_dump("Enviar al dueño");
        $message = $event->message;
        if (auth()->check()) {
            $message->email = auth()->user()->email;
            $message->nombre = auth()->user()->name;
        }
        Mail::send('emails.contact', ['msj' => $message], function($m) use ($message){
            $m->from($message->email, $message->nombre)
                ->to(config('mail.from.address'), config('mail.from.name'))
                ->subject('Tienes un nuevo mensaje');
        });
    }
}
