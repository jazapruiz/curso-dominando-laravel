@extends('layout')

@section('contenido')

<h1>Usuario {{$user->name}}</h1>

<ul class="list-unstyled">
	<li><strong>Rol</strong>: 
		{{-- <ul class="list-inline"> --}}
			@foreach ($user->roles as $role)
				{{-- <li class="list-inline-item">{{$role->display_name}}</li> --}}
				{{$role->display_name}}
			@endforeach
		{{-- </ul> --}}
	</li>
	<li><strong>Email</strong>: <a href="mailto:{{$user->email}}">{{$user->email}}</a></li>
	<li><strong>Fecha creación</strong>: {{$user->created_at}}</li>
	<li><strong>Fecha modificación</strong>: {{$user->updated_at}}</li>
</ul>

@can('edit', $user)
	<a href="{{ route('usuarios.edit', $user->id) }}" class="btn btn-primary" role="button">Editar</a>
@endcan

@can('destroy', $user)
	<form style="display: inline;" action="{{ route('usuarios.destroy', $user->id) }}" method="POST">
		{{method_field('DELETE')}}
		{{csrf_field()}}
		<input type="submit" class="btn btn-danger" value="Eliminar">
	</form>
@endcan

@stop