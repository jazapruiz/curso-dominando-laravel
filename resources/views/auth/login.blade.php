@extends('layout')

@section('contenido')
<h1>Login</h1>
<form class="form-inline" action="{{route('iniciarsesion')}}" method="post">
	{!! csrf_field() !!}
	<input type="email" class="form-control" name="email" placeholder="Email">
	<input type="password" class="form-control" name="password" placeholder="Password">
	<input type="submit" class="btn btn-primary" value="Entrar">
</form>
@stop