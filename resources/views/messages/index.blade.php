@extends('layout')

@section('contenido')

<h1>Lista de mensajes</h1>
@if(session()->has('info'))
	<div class="alert alert-success" role="alert">
	  	{{ session('info') }}
	</div>
@endif

@if(count($messages) > 0)
	<table class="table">
		<thead>
			<tr>
				<td>ID</td>
				<td>Nombre</td>
				<td>Email</td>
				<td>Mensaje</td>
				<td>Notas</td>
				<td>Etiquetas</td>
				<td>Teléfono</td>
				<td>Creado el</td>
				<td>Modificado el</td>
				<td>Acciones</td>
			</tr>
		</thead>
		<tbody>
		@foreach($messages as $message)
			<tr>
				<td>{{$message->id}}</td>
				
				<td>{{$message->present()->userName()}}</td>
				<td>{{$message->present()->userEmail()}}</td>

				<td>{{$message->present()->link()}}</td>
				<td>{{$message->present()->notes()}}</td>
				<td>{{$message->present()->tags()}}</td>
				<td>{{$message->phone}}</td>
				<td>{{$message->created_at}}</td>
				<td>{{$message->updated_at}}</td>
				<td>
					<a class="btn btn-info btn-sm" href="{{ route('mensajes.edit', $message->id) }}">Editar</a>
					<form style="display: inline;" action="{{ route('mensajes.destroy', $message->id) }}" method="POST">
						{{method_field('DELETE')}}
						{{csrf_field()}}
						<input type="submit" class="btn btn-danger btn-sm" value="Eliminar">
					</form>
				</td>
			</tr>
		@endforeach
		</tbody>
		{!! $messages->fragment('hash')->appends(request()->query())->links('pagination::bootstrap-4') !!}
	</table>
@else
	<p>No hay mensajes guardados</p>
@endif

@stop