let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Configuración por default
// mix.js('resources/assets/js/app.js', 'public/js')

// Configuración para combinar archivos JS y CSS
mix.js(
	[
		// 'node_modules/jquery/dist/jquery.js',
		// 'node_modules/bootstrap/dist/js/bootstrap.js',
		'resources/assets/js/app.js'
	], 'public/js/all.js') // Ubicación y nombre del archivo a generar
   .sass('resources/assets/sass/app.scss', 'public/css');
