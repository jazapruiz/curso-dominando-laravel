<?php

namespace App;

use App\Presenter\MessagePresenter;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    // protected $table = "nombre_de_la_tabla"; // para cambiar el nombre de la tabla
    protected $fillable = ['nombre', 'email', 'mensaje']; // campos que Eloquent insertará desde Message::create() y Message:update(). Impide la asignación masiva de datos

    public function user() {
    	return $this->belongsTo(User::class);
    }

    public function note() {
    	return $this->morphOne(Note::class, 'notable');
    }

    public function tags() {
    	return $this->morphToMany(tags::class, 'taggable');
    }

    public function present()
    {
        return new MessagePresenter($this);
    }
}
