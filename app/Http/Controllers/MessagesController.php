<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use App\Events\MessageWasReceived;
use App\Repositories\MessagesInterface;
use App\Http\Requests\CreateMessageRequest;

class MessagesController extends Controller
{
    protected $messages;

    public function __construct(MessagesInterface $messages){
        $this->messages = $messages;
        $this->middleware('auth', ['except' => ['create', 'store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = $this->messages->getPaginated();
        return view('messages.index', compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd(config('services.sparkpost.secret'));
        return view('messages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateMessageRequest $request)
    {
        $message = $this->messages->store($request);
        event(new MessageWasReceived($message));
        return redirect()
                    ->route('mensajes.create')
                    ->with('info', 'Tu mensaje ha sido enviado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $message = $this->messages->findById($id);
        return view('messages.show', compact('message'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $message = $this->messages->findById($id);
        return view('messages.edit', compact('message'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateMessageRequest $request, $id)
    {
        $message = $this->messages->update($request, $id);
        // Redireccionar
        return redirect()
                    ->route('mensajes.index')
                    ->with('info', 'Tu mensaje ha sido modificado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message = $this->messages->destroy($id);
        return redirect()
                    ->route('mensajes.index')
                    ->with('info', 'Tu mensaje ha sido eliminado correctamente');
    }
}
