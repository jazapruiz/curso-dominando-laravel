<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    protected $fillable = ['name'];

    public function message() {
    	return $this->morphByMany(Message::class, 'taggable');
    }

    public function user() {
    	return $this->morphByMany(User::class, 'taggable');
    }
}
