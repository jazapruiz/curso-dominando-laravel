<?php

use App\User;
use App\Role;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        Role::truncate();
        DB::table('assigned_roles')->truncate();

        $user = User::create([
        	'name'=>"José Alexander Zapata Ruiz",
        	'email'=>"azapata@turismociva.com",
        	'password'=>"asdasd"
        ]);

        $role = Role::create([
        	'name'=>"admin",
        	'display_name'=>"Administrador del sitio",
        	'description'=>"Este rol tiene los permisos de administrar el sitio entero"
        ]);

        $user->roles()->save($role);
    }
}
