@extends('layout')

@section('contenido')
<h1>Contacto</h1>
<form method="post" action="{{route('mensajes.update', $message->id)}}">
	{{ method_field('PUT') }}
	@include('messages.form', ['btnText' => 'Modificar', 'stateFields' => !$message->user_id])
</form>
@stop