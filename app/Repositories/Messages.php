<?php 

namespace App\Repositories;

use App\Message;

/**
 * 
 */
class Messages implements MessagesInterface
{
	
	function __construct()
	{
		# code...
	}

	public function getPaginated(){
        return Message::with(['user', 'note', 'tags'])
                    ->orderBy('created_at', request('sorted', 'DESC'))
                    ->paginate(10);
	}

	public function store($request) {
		// Guardar el mensaje
        // DB::table('messages')->insert([
        //     'nombre'=>$request->input('nombre'),
        //     'email'=>$request->input('email'),
        //     'mensaje'=>$request->input('mensaje'),
        //     'created_at'=>Carbon::now(),
        //     'updated_at'=>Carbon::now()
        // ]);

        // Primera forma de usar un modelo Eloquent
        // $message = new Message;
        // $message->nombre = $request->input('nombre');
        // $message->email = $request->input('email');
        // $message->mensaje = $request->input('mensaje');
        // $request->save();

        // dd($request->all());

        // Segunda forma de usar un modelo Eloquent
        $message = Message::create($request->all());
        if (auth()->check()) {
            auth()->user()->messages()->save($message);
        }
        
        return $message;
	}

	public function findById($id) {
        // $message = DB::table('messages')
        //              ->select('id', 'nombre', 'email', 'mensaje', 'phone', 'created_at', 'updated_at')
        //              ->where('id', $id)
        //              ->first();
        return Message::findOrFail($id);
	}

	public function update($request, $id) {
		// Guardar el mensaje
        // DB::table('messages')->where('id', $id)->update([
        //     'nombre'=>$request->input('nombre'),
        //     'email'=>$request->input('email'),
        //     'mensaje'=>$request->input('mensaje'),
        //     'updated_at'=>Carbon::now()
        // ]);
        return Message::findOrFail($id)->update($request->all());

	}

	public function destroy($id) {
		// DB::table('messages')->where('id', $id)->delete();
        return Message::findOrFail($id)->delete();
	}
}