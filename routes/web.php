<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// DB::listen(function($query){
// 	echo "<pre>{$query->sql}</pre>";
// });

// Route::get('job', function() {
//     dispatch(new App\Jobs\SendEmail);
//     return 'Listo!';
// });

// Route::get('{URL}'), ['as' => '{NOMBRE}', function () {
Route::get('/', ['as' => 'home', 'uses' => 'PagesController@home']);
Route::get('createuser', ['as' => 'createuser', 'uses' => 'PagesController@createuser']);
Route::get('home', 'PagesController@home');
Route::get('saludos/{nombre?}', ['as' => 'saludo', 'uses' => 'PagesController@saludo'])->where('nombre', "[A-Za-z]+");
Route::resource('mensajes', 'MessagesController');
Route::resource('usuarios', 'UsersController');
// Route::get('login', 'Auth\LoginController@showLoginForm');
Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login', ['as' => 'iniciarsesion', 'uses' => 'Auth\LoginController@login']);
Route::get('logout', ['as' => 'cerrarsesion', 'uses' => 'Auth\LoginController@logout']);

// Route::get('mensajes/create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
// Route::post('mensajes', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
// Route::get('mensajes', ['as' => 'messages.index', 'uses' => 'MessagesController@index']);
// Route::get('mensajes/{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
// Route::get('mensajes/{id}/edit', ['as' => 'messages.edit', 'uses' => 'MessagesController@edit']);
// Route::put('mensajes/{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
// Route::delete('mensajes/{id}', ['as' => 'messages.destroy', 'uses' => 'MessagesController@destroy']);