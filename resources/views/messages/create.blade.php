@extends('layout')

@section('contenido')
<h1>Contacto</h1>
<h2>Escríbeme</h2>
@if(session()->has('info'))
	<div class="alert alert-success" role="alert">
	  	{{ session('info') }}
	</div>
{{-- @else --}}
@endif
<form method="post" action="{{route('mensajes.store')}}">
	@include('messages.form', ['message' => new App\Message, 'stateFields' => Auth::guest()])
</form>
@stop