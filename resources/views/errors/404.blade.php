@extends('layout')

@section('contenido')
	<small>URL: {{ request()->url() }}</small>
	<h3 class="text-danger">La página que esta solicitando no encuentra disponible</h3>
	<a class="btn btn-primary" href="{{ route('home') }}">Regresar</a>
@stop