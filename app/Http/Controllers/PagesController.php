<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\CreateMessageRequest;

class PagesController extends Controller
{
    public function __construct(){
        $this->middleware('example', ['only' => ['home']]);
    }

    public function home() {
        // $texto = "17b9c461-807a-459e-93ee-647bb6143261";
        // echo $texto."<br>";
        // $nuevo_texto = "151vc067ia0692-".$texto;
        // echo $nuevo_texto;

        // dd(base64_encode($nuevo_texto));
        return view('home');
    }

    public function createuser() {
    	$user = new User;
        $user->name = "Estudiante";
        $user->email = "estudiante@email.com";
        $user->password = bcrypt("asdasd");
        $user->save();

        return $user;
    }

    public function saludo($nombre = "invitado") {
    	$html = "<h2>htmlentities</h2>"; // Imaginemos que viene de un formulario
		$script = "<script>alert('Código malicioso! Peligro!!!')</script>"; // Imaginemos que viene de un formulario
		$consolas = ["Play Station 4", "X Box 360", "Wii U", "Nintendo DS"];
	    return view('saludo', compact('nombre', 'html', 'script', 'consolas'));
    }
}
