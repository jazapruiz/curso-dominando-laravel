{{-- Devuelve un input con el valor del _token --}}
{!! csrf_field() !!}
@if ($stateFields)
	<p><label for="nombre">
		Nombre
		<input class="form-control" type="text" name="nombre" value="{{ $message->nombre or old('nombre') }}">
		{!! $errors->first('nombre', '<span class="p-1 mb-1 bg-danger text-white">:message</span>') !!}
	</label></p>
	<p><label for="email">
		Email
		<input class="form-control" type="text" name="email" value="{{ $message->email or old('email') }}">
		{!! $errors->first('email', '<span class="p-1 mb-1 bg-danger text-white">:message</span>') !!}
	</label></p>
@endif
<p><label for="mensaje">
	Mensaje
	<textarea class="form-control" name="mensaje">{{ $message->mensaje or old('mensaje') }}</textarea>
	{!! $errors->first('mensaje', '<span class="p-1 mb-1 bg-danger text-white">:message</span>') !!}
</label></p>
<input type="submit" class="btn btn-primary" value="{{ $btnText or 'Enviar' }}">

{{-- Sintaxis --}}
{{-- PHP<7	: isset($a) ? $a : 0 --}}
{{-- PHP 7	: $a ?? 0 --}}
{{-- LARAVEL: $a or 0 --}}