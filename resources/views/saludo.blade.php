@extends('layout')

@section('contenido')
<h1>Saludo para {{ $nombre }}</h1>
<ul>
	@forelse($consolas as $consola)
		<li>{{ $consola }}</li>
	@empty
		<p>No hay consolas :(</p>
	@endforelse
</ul>
@if(count($consolas) === 1)
	<p>Solo tienes una consola</p>
@else
	<p>Tienes {{ count($consolas) }} consolas</p>
@endif
{!! $html !!}
<!-- {{ $script }} -->
<!-- {!! $script !!} -->
@stop