@extends('layout')

@section('contenido')
<h1>Mensaje {{$message->id}}</h1>
<p>Mensaje de {{$message->present()->userName()}} - {{$message->present()->userEmail()}}</p>
<p>{{$message->mensaje}}</p>
@stop