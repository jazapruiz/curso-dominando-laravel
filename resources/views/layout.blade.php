<!DOCTYPE html>
<html>
<head>
	<title>Mi sitio Web</title>
	<link rel="stylesheet" href="/css/app.css">
	<script>
		window.Laravel = {
			csrfToken: "{{csrf_token()}}"
		}
	</script>
	<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  	<a class="navbar-brand" href="#">Blog.test</a>
		  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    	<span class="navbar-toggler-icon"></span>
		  	</button>
		  	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		    	<ul class="navbar-nav mr-auto">
		      		<li class="nav-item {{ activeMenu(request()->is("/")) }}">
		        		<a class="nav-link" href="{{ route('home') }}">Inicio</a>
		      		</li>
		      		<li class="nav-item {{ activeMenu(request()->is("saludos/*")) }}">
		        		<a class="nav-link" href="{{ route('saludo', 'ale') }}">Saludos</a>
		      		</li>
		      		<li class="nav-item {{ activeMenu(request()->is("mensajes/create")) }}">
		        		<a class="nav-link" href="{{ route('mensajes.create') }}">Contáctenos</a>
		      		</li>
		      		@if(auth()->check())
		      		<li class="nav-item {{ activeMenu(request()->is("mensajes*")) }}">
						<a class="nav-link" href="{{ route('mensajes.index') }}">Mensajes</a>
					</li>
			      	@if(auth()->user()->hasRole(['admin']))
			      		<li class="nav-item {{ activeMenu(request()->is("usuarios*")) }}">
							<a class="nav-link" href="{{ route('usuarios.index') }}">Usuarios</a>
						</li>
			      	@endif
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  {{ auth()->user()->name }}
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						  {{-- <a class="dropdown-item" href="#">Action</a> --}}
						  <a class="dropdown-item" href="{{ route('usuarios.edit', auth()->id()) }}">Mi cuenta</a>
						  <div class="dropdown-divider"></div>
						  <a class="dropdown-item" href="{{route('cerrarsesion')}}">Cerrar sesión</a>
						</div>
					</li>
					@endif
		      		@if(auth()->guest())
		      		<li class="nav-item {{ activeMenu(request()->is("login")) }}">
						<a class="nav-link" href="{{route('login')}}">Login</a>
					</li>
					@endif
		    	</ul>
		  	</div>
		</nav>
	</header>
	<div class="container-fluid">
		@yield('contenido')
	</div>
	<hr>
	<footer>
		<div class="container-fluid">
			Copyright {{ date('Y') }}
		</div>
	</footer>
	<script type="text/javascript" src="/js/all.js"></script>
</body>
</html>