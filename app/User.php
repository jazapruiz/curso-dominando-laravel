<?php

namespace App;

use App\Presenter\UserPresenter;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Esta función encrypta el password cuando se haga un update o insert a la tabla User
     */
    public function setPasswordAttribute($password) {
        $this->attributes['password'] = bcrypt($password);
    }

    /**
     * Esta función indica la relación con la tabla Roles
     */
    public function roles() {
        return $this->belongsToMany(Role::class, 'assigned_roles'); // 'assigned_roles' = Nombre de la tabla en la BD
    }

    public function hasRole(array $roles) {
        return $this->roles->pluck('name')->intersect($roles)->count();
        // ^
        // |
        // Esto es igual a esto
        //                  |
        //                  v
        // foreach ($roles as $role) {
        //     foreach ($this->roles as $userRole) {
        //         if ($userRole->name === $role) {
        //             return true;
        //         }
        //     }
        // }
        // return false;
    }

    public function isAdmin() {
        return $this->hasRole(['admin']);
    }

    public function messages() {
        return $this->hasMany(Message::class);
    }

    public function note() {
        return $this->morphOne(Note::class, 'notable');
    }

    public function tags() {
        return $this->morphToMany(tags::class, 'taggable')->withTimestamps();
    }

    public function present()
    {
        return new UserPresenter($this);
    }
}
