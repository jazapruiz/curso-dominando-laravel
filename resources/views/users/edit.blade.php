@extends('layout')

@section('contenido')
<h1>Editar usuario</h1>
<form method="post" action="{{route('usuarios.update', $user->id)}}">
	{{ method_field('PUT') }}
	@include('users.form')
	<input type="submit" class="btn btn-primary" value="Modificar">
</form>
@stop