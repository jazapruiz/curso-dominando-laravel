@extends('layout')

@section('contenido')
<h1>Crear usuario</h1>
<form method="post" action="{{route('usuarios.store')}}">
	@include('users.form', ['user' => new App\User])
	<input type="submit" class="btn btn-primary" value="Guardar">
</form>
@stop