{{-- Devuelve un input con el valor del _token --}}
{!! csrf_field() !!}
<p><label for="name">
	Nombre
	<input type="text" class="form-control" name="name" value="{{ $user->name ?? old('name') }}">
	{!! $errors->first('name', '<span class="p-1 mb-1 bg-danger text-white">:message</span>') !!}
</label></p>
<p><label for="email">
	Email
	<input type="text" class="form-control" name="email" value="{{ $user->email ?? old('email') }}">
	{!! $errors->first('email', '<span class="p-1 mb-1 bg-danger text-white">:message</span>') !!}
</label></p>
@unless ($user->id)
	<p><label for="password">
		Clave
		<input type="password" class="form-control" name="password" value="">
		{!! $errors->first('password', '<span class="p-1 mb-1 bg-danger text-white">:message</span>') !!}
	</label></p>
	<p><label for="password_confirmation">
		Confirmar clave
		<input type="password" class="form-control" name="password_confirmation" value="">
		{!! $errors->first('password_confirmation', '<span class="p-1 mb-1 bg-danger text-white">:message</span>') !!}
	</label></p>
@endunless
@foreach ($roles as $id => $role)
	<div class="form-group form-check">
	    <input type="checkbox" class="form-check-input" 
	    	name="role[]" 
	    	{{$user->roles->pluck('id')->contains($id) ? 'checked' : ''}}
	    	value="{{$id}}" 
	    	id="exampleCheck{{$id}}">
	    <label class="form-check-label" for="exampleCheck{{$id}}">{{$role}}</label>
	</div>
@endforeach
<p>{!! $errors->first('role', '<span class="p-1 mb-1 bg-danger text-white">:message</span>') !!}</p>