@extends('layout')

@section('contenido')

<h1>Lista de usuarios</h1>
@if(session()->has('info'))
	<div class="alert alert-success" role="alert">
	  	{{ session('info') }}
	</div>
@endif
<p><a class="btn btn-primary" href="{{route('usuarios.create')}}">Crear nuevo usuario</a></p>
@if(count($users) > 0)
	<table class="table">
		<tr>
			<td>ID</td>
			<td>Nombre</td>
			<td>Email</td>
			<td>Rol</td>
			<td>Notas</td>
			<td>Etiquetas</td>
			<td>Creado el</td>
			<td>Modificado el</td>
			<td>Acciones</td>
		</tr>
		@foreach($users as $user)
			<tr>
				<td>{{$user->id}}</td>
				<td>{{$user->present()->link()}}</td>
				<td>{{$user->email}}</td>
				<td>
					{{$user->present()->roles()}}
				</td>
				<td>{{$user->present()->notes()}}</td>
				<td>{{$user->present()->tags()}}</td>
				<td>{{$user->created_at}}</td>
				<td>{{$user->updated_at}}</td>
				<td>
					{{-- @if($user->role !== "admin") --}}
					<a class="btn btn-info btn-sm" href="{{ route('usuarios.edit', $user->id) }}">Editar</a>
					<form style="display: inline;" action="{{ route('usuarios.destroy', $user->id) }}" method="POST">
						{{method_field('DELETE')}}
						{{csrf_field()}}
						<input type="submit" class="btn btn-danger btn-sm" value="Eliminar">
					</form>
					{{-- @endif --}}
				</td>
			</tr>
		@endforeach
	</table>
@else
	<p>No hay mensajes guardados</p>
@endif

@stop